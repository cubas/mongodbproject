package br.cubas.mongodb;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.set;
import static com.mongodb.client.model.Filters.and;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import org.bson.Document;
import org.bson.types.ObjectId;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;

public class BolsaFamiliaDAO {
	
	private final String host = "localhost";
	private final int port = 27017;
	private final String database = "bolsafamilia";
	private final String collection = "pagamentos";

	private MongoCollection<Document> bolsafamiliaCollection;
	private MongoClient mongoClient;

	public BolsaFamiliaDAO() {
		try {
			mongoClient = new MongoClient(host, port);
			MongoDatabase db = mongoClient.getDatabase(database);
			bolsafamiliaCollection = db.getCollection(collection);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<BolsaFamilia> findAll(String filtro) {
		
		MongoCursor<Document> cursor = bolsafamiliaCollection.find(Filters.regex("nome",
				Pattern.compile(".*"+filtro+".*" , Pattern.CASE_INSENSITIVE)))
				.sort(new BasicDBObject("nome",1))
				.iterator();

		List<BolsaFamilia> bolsasFamilia = new ArrayList<BolsaFamilia>();
		while (cursor.hasNext()) {
			Document resultElement = cursor.next();

			BolsaFamilia bolsaFamilia = new BolsaFamilia();
			bolsaFamilia.setId((ObjectId) resultElement.get("_id"));
			bolsaFamilia.setUf((String) resultElement.get("uf"));
			bolsaFamilia.setMunicipio((String) resultElement.get("municipio"));
			bolsaFamilia.setNome((String) resultElement.get("nome"));
			bolsaFamilia.setPrograma((String) resultElement.get("programa"));
			bolsaFamilia.setValor((Double) resultElement.get("valor"));
			bolsaFamilia.setData((Date) resultElement.get("data"));
			
			bolsasFamilia.add(bolsaFamilia);

			System.out.println("Bolsa Familia lido = " + bolsaFamilia);
		}

		return bolsasFamilia;
	}
	
	
	public List<BolsaFamilia> findAll() {

		List<BolsaFamilia> bolsasFamilia = new ArrayList<BolsaFamilia>();
		MongoCursor<Document> cursor = bolsafamiliaCollection.find()
				.sort(new BasicDBObject("nome",1))
				.iterator();

		while (cursor.hasNext()) {
			Document resultElement = cursor.next();

			BolsaFamilia bolsaFamilia = new BolsaFamilia();
			bolsaFamilia.setId((ObjectId) resultElement.get("_id"));
			bolsaFamilia.setUf((String) resultElement.get("uf"));
			bolsaFamilia.setMunicipio((String) resultElement.get("municipio"));
			bolsaFamilia.setNome((String) resultElement.get("nome"));
			bolsaFamilia.setPrograma((String) resultElement.get("programa"));
			bolsaFamilia.setValor((Double) resultElement.get("valor"));
			bolsaFamilia.setData((Date) resultElement.get("data"));
			
			bolsasFamilia.add(bolsaFamilia);

			System.out.println("Bolsa Familia lido = " + bolsaFamilia);
		}

		return bolsasFamilia;
	}
	
	public BolsaFamilia findByUfMunicipio(final String uf, final String municipio) {
		Document documento = bolsafamiliaCollection.find(and(eq("uf", uf), eq("municipio", municipio))).first();
		System.out.println("BolsaFamilia lido = " + documento);
		
		BolsaFamilia bolsaFamilia = new BolsaFamilia();
		bolsaFamilia.setId((ObjectId) documento.get("_id"));
		bolsaFamilia.setUf((String) documento.get("uf"));
		bolsaFamilia.setMunicipio((String) documento.get("municipio"));
		bolsaFamilia.setNome((String) documento.get("nome"));
		bolsaFamilia.setPrograma((String) documento.get("programa"));
		bolsaFamilia.setValor((Double) documento.get("valor"));
		bolsaFamilia.setData((Date) documento.get("data"));
		return bolsaFamilia;
	} 
	
	public BolsaFamilia findById(final String id) {

		BolsaFamilia bolsaFamilia = new BolsaFamilia();
		System.out.println("busca por id = " + id);
		Document documento = bolsafamiliaCollection.find(eq("_id",new ObjectId(id))).first();

		System.out.println("BolsaFamilia lido = " + documento);
		bolsaFamilia.setId((ObjectId) documento.get("_id"));
		bolsaFamilia.setUf((String) documento.get("uf"));
		bolsaFamilia.setMunicipio((String) documento.get("municipio"));
		bolsaFamilia.setNome((String) documento.get("nome"));
		bolsaFamilia.setPrograma((String) documento.get("programa"));
		bolsaFamilia.setValor((Double) documento.get("valor"));
		bolsaFamilia.setData((Date) documento.get("data"));
		return bolsaFamilia;
	}

	public void insert(BolsaFamilia bolsaFamilia) {
		Document bolsaFamiliaParaGravar = new Document();
		bolsaFamiliaParaGravar.append("uf",bolsaFamilia.getUf());
		bolsaFamiliaParaGravar.append("municipio",bolsaFamilia.getMunicipio());
		bolsaFamiliaParaGravar.append("nome",bolsaFamilia.getNome());
		bolsaFamiliaParaGravar.append("programa",bolsaFamilia.getPrograma());
		bolsaFamiliaParaGravar.append("valor",bolsaFamilia.getValor());
		bolsaFamiliaParaGravar.append("data",bolsaFamilia.getData());
		bolsafamiliaCollection.insertOne(bolsaFamiliaParaGravar);
		System.out.println("novo BolsaFamilia = " + bolsaFamilia);
	}

	public void update(BolsaFamilia bolsaFamilia) {
		bolsafamiliaCollection.updateOne(eq("_id",bolsaFamilia.getId()),set("uf", bolsaFamilia.getUf()));
		bolsafamiliaCollection.updateOne(eq("_id",bolsaFamilia.getId()),set("municipio", bolsaFamilia.getMunicipio()));
		bolsafamiliaCollection.updateOne(eq("_id",bolsaFamilia.getId()),set("nome", bolsaFamilia.getNome()));
		bolsafamiliaCollection.updateOne(eq("_id",bolsaFamilia.getId()),set("programa", bolsaFamilia.getPrograma()));
		bolsafamiliaCollection.updateOne(eq("_id",bolsaFamilia.getId()),set("valor", bolsaFamilia.getValor()));
		bolsafamiliaCollection.updateOne(eq("_id",bolsaFamilia.getId()),set("data", bolsaFamilia.getData()));
		System.out.println("BolsaFamilia alterado = " + bolsaFamilia);
	}

	public void remove(String id) {
		bolsafamiliaCollection.deleteOne(eq("_id",new ObjectId(id)));
		System.out.println("BolsaFamilia removido = " + id);
	}
	
public List<BolsaFamiliaAgregada> agregation(String agregation){
		
		List<BolsaFamiliaAgregada> bolsasAgregadas = new ArrayList<BolsaFamiliaAgregada>();
		
		Block<Document> printBlock = new Block<Document>() {
			@Override
			public void apply(final Document document) {
				BolsaFamiliaAgregada bolsa = new BolsaFamiliaAgregada();
				bolsa.setId(document.getString("_id"));
				bolsa.setTotal(document.getInteger("total"));
				bolsa.setSoma(document.getDouble("soma"));
				bolsa.setMedia(document.getDouble("media"));
				
				bolsasAgregadas.add(bolsa);
			}
		};

		bolsafamiliaCollection.aggregate(
				Arrays.asList(
								
								Aggregates.group("$"+agregation, 
										Accumulators.sum("total", 1),
										Accumulators.sum("soma", "$valor"), 
										Accumulators.avg("media", "$valor")
										
										),
								
								Aggregates.sort(new BasicDBObject("total",1)),
								
								Aggregates.project(Projections.fields(
										Projections.include("total"),
										Projections.include("soma"),
										Projections.include("media")
										))
								
						)
				).allowDiskUse(true).forEach(printBlock);
		
		return bolsasAgregadas;
	}

	
	public List<BolsaFamiliaAgregada> agregation(String agregation, String filtro){
		
		List<BolsaFamiliaAgregada> bolsasAgregadas = new ArrayList<BolsaFamiliaAgregada>();
		
		Block<Document> printBlock = new Block<Document>() {
			@Override
			public void apply(final Document document) {
				BolsaFamiliaAgregada bolsa = new BolsaFamiliaAgregada();
				bolsa.setId(document.getString("_id"));
				bolsa.setTotal(document.getInteger("total"));
				bolsa.setSoma(document.getDouble("soma"));
				bolsa.setMedia(document.getDouble("media"));
				
				bolsasAgregadas.add(bolsa);
			}
		};

		bolsafamiliaCollection.aggregate(
				Arrays.asList(
								Aggregates.match(Filters.regex(agregation,
										Pattern.compile(".*"+filtro+".*" , Pattern.CASE_INSENSITIVE))),
								
								Aggregates.group("$"+agregation, 
										Accumulators.sum("total", 1),
										Accumulators.sum("soma", "$valor"), 
										Accumulators.avg("media", "$valor")
										
										),
								Aggregates.sort(new BasicDBObject("total",1)),
								
								Aggregates.project(Projections.fields(
										Projections.include("total"),
										Projections.include("soma"),
										Projections.include("media")
										))
						)
				).allowDiskUse(true).forEach(printBlock);
		
		return bolsasAgregadas;
	}

}
