package br.cubas.mongodb;

public class BolsaFamiliaAgregada {
	
	private String id;
	private Integer total;
	private Double soma;
	private Double media;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public Integer getTotal() {
		return total;
	}
	
	public void setTotal(Integer total) {
		this.total = total;
	}
	
	public Double getSoma() {
		return soma;
	}
	
	public void setSoma(Double soma) {
		this.soma = soma;
	}
	
	public Double getMedia() {
		return media;
	}
	
	public void setMedia(Double media) {
		this.media = media;
	}

	@Override
	public String toString() {
		return "BolsaFamiliaAgregada [id=" + id + ", total=" + total + ", soma=" + soma + ", media=" + media + "]";
	}
	
}
